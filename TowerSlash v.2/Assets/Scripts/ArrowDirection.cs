using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction{

    right, 
    left,
    down, 
    up
}

public class ArrowDirection : MonoBehaviour
{

   public Direction direction;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TouchInputReference : MonoBehaviour
{   
    // code was repurposed from previous project

    //this is where we put the inputs
    public Vector2 SwipeDelta { get; private set;}
    public Vector2  StartTouch;
    public bool isDragging = false;
    public bool SwipeUp { get; private set;}
    public bool SwipeDown { get; private set;}
    public bool SwipeLeft { get; private set;}
    public bool SwipeRight { get; private set;}
    public bool Tap { get; private set;}

    void Update(){

        Tap = SwipeUp = SwipeDown = SwipeRight = SwipeLeft = false;

        #region  Standalone Inputs

        if (Input.GetMouseButtonDown(0)){

            Tap = true;
            isDragging = true;
            StartTouch = Input.mousePosition;
        }

        else if (Input.GetMouseButtonUp(0)){

            isDragging = false;
            Reset();
        }
        #endregion

        #region Mobile Inputs

        if (Input.touches.Length > 0){ // one or more touchs on screen
        
            if (Input.touches[0].phase == TouchPhase.Began){

                Tap = true;
                isDragging = true;
                StartTouch = Input.touches[0].position;
            }

            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled){

                isDragging = false;
                Reset();
            }
        }
        #endregion

        CalculateSwipeDirection();

    }

    private void CalculateSwipeDirection(){

        SwipeDelta = Vector2.zero;

        if (isDragging){

            if (Input.touches.Length > 0){

                SwipeDelta = Input.touches[0].position - StartTouch;
            }

            else if(Input.GetMouseButton(0)){

                SwipeDelta = (Vector2)Input.mousePosition - StartTouch;
            }
        }

        if (SwipeDelta.magnitude > 100){

            float x = SwipeDelta.x;
            float y = SwipeDelta.y; 

            if (Mathf.Abs(x) > Mathf.Abs(y)){ // horizontal movement

            if (x > 0){

                SwipeRight = true;
                Debug.Log("Right");
            }

            else {

                SwipeLeft = true;
                Debug.Log("Left");
            }

            }

            else { // vertical movement

            if (y > 0){

                SwipeUp = true;
                Debug.Log("Up");
            }

            else {

                SwipeDown = true;
                Debug.Log("Down");
            }
            }

            Reset();
        }
    }

    private void Reset(){

        StartTouch = SwipeDelta = Vector2.zero;
        isDragging = false;
    }
}

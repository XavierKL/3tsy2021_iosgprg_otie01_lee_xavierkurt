using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public enum EnemyType{

    red,
    green,
    gray
}

public class Enemy : MonoBehaviour
{

    public EnemyType enemyType;
    public GameObject[] arrows;
    GameObject currentArrow;
    bool hitPlayer;
    bool isNear;
    int randomNumber;
    
    public PowerUpDrop powerUp;

    #region Unity Functions
    void Start(){

        randomNumber = Random.Range(0, arrows.Length);

        isNear = false;
        hitPlayer = false;

        switch(enemyType){

            case EnemyType.red:
            arrows[randomNumber].SetActive(true);
            break;

            case EnemyType.green:
            arrows[randomNumber].SetActive(true);
            break;

            case EnemyType.gray:
            StartCoroutine(RandomArrow());
            break;

            default:
            break;
        }
        
    }

    public void OnTriggerEnter2D (Collider2D col){

        if(col.gameObject.tag == "Player Range"){

            isNear = true;
        }

        if(col.gameObject.tag == "Clear"){

            Destroy(this.gameObject);
        }
    }

    public void OnColliderEnter2D(Collision2D col){

        if(col.gameObject.tag == "Player"){

            hitPlayer = true;
        }
    }
    void OnDestroy(){

        if (hitPlayer){

            int i = Random.Range(0, 100);

            if (i >= powerUp.minProbability && i <= powerUp.maxProbability){

                Instantiate(powerUp.healhPowerUp, transform.position, Quaternion.identity);
            }
        }
    }
    #endregion

    #region Enemy Functions
    
    public IEnumerator RandomArrow(){

        while(true){

            randomNumber = Random.Range(0, arrows.Length);
            currentArrow = arrows[randomNumber];
            currentArrow.SetActive(true);
            yield return new WaitForSeconds(.5f);

            if(isNear){
                
                break;
            }

            currentArrow.SetActive(false);
        }

        yield return new WaitForEndOfFrame();
    }
    
    [System.Serializable]
    public class PowerUpDrop{

        public GameObject healhPowerUp;
        public int minProbability = 0;
        public int maxProbability = 0;
    }
    #endregion
}

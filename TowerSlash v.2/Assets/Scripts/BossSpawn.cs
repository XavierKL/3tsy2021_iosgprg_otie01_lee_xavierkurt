using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawn : MonoBehaviour
{

    public Spawner spawner;
    public PlayerInput playerInputReference;
    public bool hasSpawned;
    // Start is called before the first frame update
    void Start()
    {
        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<Spawner>();
        playerInputReference = GameObject.FindGameObjectWithTag("Player Range").GetComponent<PlayerInput>();
    }

    void OnDestroy(){

        spawner.spawnBoss = false;
        spawner.bossIsAlive = false;
        playerInputReference.bossSpawned = false;
        spawner.hitsBeforeDeath = 0;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour
{
    
    //code sourced from https://www.youtube.com/watch?v=HIsEqKPoJXM

    public GameObject[] floor;
    Transform playerTransform;
    float spawnY = 0.0f;
    float floorLength = 8f;
    float safeZone = 10f;
    private int floorsOnScreen = 3;
    int lastPrefabIndex = 0;

    List<GameObject> activeFloor;

    // Start is called before the first frame update
    void Start()
    {

        activeFloor = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        for(int i = 0; i < floorsOnScreen; i++){

            floorSpawn();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (playerTransform.transform.position.y - safeZone > (spawnY - floorsOnScreen * floorLength)){

            floorSpawn();
            floorDelete();
        }
    }

    void floorSpawn(int prefabIndex = -1){

        GameObject obj;

        if(prefabIndex == -1) obj = Instantiate(floor[RandomPrefabIndex()]) as GameObject;
        else obj = Instantiate(floor[prefabIndex]) as GameObject;
        
        obj.transform.SetParent(transform);
        obj.transform.position = Vector3.up * spawnY;
        spawnY += floorLength;
        activeFloor.Add(obj);
    }

    void floorDelete(){

        Destroy(activeFloor[0]);
        activeFloor.RemoveAt(0);
    }

    int RandomPrefabIndex(){

        if(floor.Length <= 1) return 0;

        int randomIndex = lastPrefabIndex;

        while(randomIndex == lastPrefabIndex){

            randomIndex = Random.Range(0, floor.Length);
        }

        lastPrefabIndex = randomIndex;
        return randomIndex;
    }
}

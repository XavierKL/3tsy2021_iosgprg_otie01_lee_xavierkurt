using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerInput : MonoBehaviour
{

    //code repurposed from previous projects
    public TouchInputReference touchInputs;
    public Player playerScript;
    public GameObject currentEnemy; // holder for enemy
    public Enemy enemyReference;
    public ArrowDirection arrowDirection;
    public TextMeshProUGUI scoreText;
    public Spawner spawnReference;
    public int score = 0;
    int scoreAfterKill;
    public bool bossSpawned;
    #region Unity Functions
    // Start is called before the first frame update
    void Start()
    {
        touchInputs = this.GetComponent<TouchInputReference>();
        playerScript = this.GetComponentInParent<Player>();
        scoreText.text = score.ToString();
        spawnReference = GameObject.FindGameObjectWithTag("Spawner").GetComponent<Spawner>();
        bossSpawned = false;
    }

    void OnTriggerEnter2D(Collider2D col){

        if (col.gameObject.tag == "Enemy"){

            // vec3 distance() - to check distance between to game obejects
            this.currentEnemy = col.gameObject;
            enemyReference = col.gameObject.GetComponent<Enemy>();
            arrowDirection = col.gameObject.GetComponentInChildren<ArrowDirection>();

            Debug.Log(enemyReference.enemyType);
            Debug.Log(arrowDirection.direction);
        }

        if (col.gameObject.tag == "Boss"){

            this.currentEnemy = col.gameObject;
            arrowDirection = col.gameObject.GetComponentInChildren<ArrowDirection>();
        }
    }
    // Update is called once per frame
    void Update()
    {

        if (!bossSpawned) scoreAfterKill = 20;
        else scoreAfterKill = 0;

        if (touchInputs.SwipeLeft){

            if (/*(enemyReference.enemyType == EnemyType.green || enemyReference.enemyType == EnemyType.red || enemyReference.enemyType == EnemyType.gray) && */arrowDirection.direction == Direction.left){

                /*if (!bossSpawned) scoreAfterKill = 10;
                else scoreAfterKill = 0;*/

                switch(enemyReference.enemyType){

                    case EnemyType.green:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.red:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.gray:
                    score += scoreAfterKill;
                    break;
                    
                    default:
                    break;
                }

                if(bossSpawned) spawnReference.hitsBeforeDeath++;
                Destroy(this.currentEnemy);
            }
        }

        if (touchInputs.SwipeRight){

            if (/*(enemyReference.enemyType == EnemyType.green || enemyReference.enemyType == EnemyType.red || enemyReference.enemyType == EnemyType.gray) && */arrowDirection.direction == Direction.right){

                // if (!bossSpawned) scoreAfterKill = 10;
                // else scoreAfterKill = 0;
                
                switch(enemyReference.enemyType){

                    case EnemyType.green:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.red:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.gray:
                    score += scoreAfterKill;
                    break;
                    
                    default:
                    break;
                }
                
                if(bossSpawned) spawnReference.hitsBeforeDeath++;
                Destroy(this.currentEnemy);
            }
            
            // else {

            //     score--;
            // }
        }

        if (touchInputs.SwipeDown){

            if (/*(enemyReference.enemyType == EnemyType.green || enemyReference.enemyType == EnemyType.red || enemyReference.enemyType == EnemyType.gray) && */arrowDirection.direction == Direction.down){

                //use distance to check if enemy is near enough 
                // if (!bossSpawned) scoreAfterKill = 10;
                // else scoreAfterKill = 0;
                switch(enemyReference.enemyType){

                    case EnemyType.green:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.red:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.gray:
                    score += scoreAfterKill;
                    break;
                    
                    default:
                    break;
                }
                
                if(bossSpawned) spawnReference.hitsBeforeDeath++;
                Destroy(this.currentEnemy);
            }

            // else {

            //     score--;
            // }
        }

        if (touchInputs.SwipeUp){

            if (/*(enemyReference.enemyType == EnemyType.green || enemyReference.enemyType == EnemyType.red || enemyReference.enemyType == EnemyType.gray) && */arrowDirection.direction == Direction.up){

                // if (!bossSpawned) scoreAfterKill = 10;
                // else scoreAfterKill = 0;
                switch(enemyReference.enemyType){

                    case EnemyType.green:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.red:
                    score += scoreAfterKill;
                    break;

                    case EnemyType.gray:
                    score += scoreAfterKill;
                    break;
                    
                    default:
                    break;
                }
                
                if(bossSpawned) spawnReference.hitsBeforeDeath++;
                Destroy(this.currentEnemy);
            }

            // else if((enemyReference.enemyType == EnemyType.green || enemyReference.enemyType == EnemyType.red || enemyReference.enemyType == EnemyType.gray) && arrowDirection.direction != Direction.up){

            //     score--; 
            // }

        }
    
        if (score > PlayerPrefs.GetInt("HighScore", 0)){

            PlayerPrefs.SetInt("HighScore", score);
        }

        scoreText.text = score.ToString();    

        if (score > 0 && score % 1000 == 0){

            bossSpawned = true;
        }

    }
    #endregion
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{

    public GameObject[] arrow;
    int randomNumber;
    // Start is called before the first frame update
    void Start()
    {

        randomNumber = Random.Range(0, arrow.Length);
        arrow[randomNumber].SetActive(true);

    }
}

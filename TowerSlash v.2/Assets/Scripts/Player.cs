using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Player : MonoBehaviour
{


    public float movementSpeed = 0f;
    public TextMeshProUGUI playerHealth;
    int playerMaxLife = 5;
    public int playerLife;
    public GameObject[] players;
    #region unity functions
    // Start is called before the first frame update
    void Start()
    {
        int charNum = PlayerPrefs.GetInt("selectedCharacter");
        playerHealth.text = playerLife.ToString();
        switch(charNum){

            case 0:
            movementSpeed = 2.5f;
            break;

            case 1:
            movementSpeed = 5.0f;
            break;

            default:
            break;
        }
        players[charNum].SetActive(true);    
        playerLife = 3;
    }

    void Update(){

        if (playerLife <= 0){

            Die();
        }

        playerHealth.text = playerLife.ToString();
    }

    void FixedUpdate(){

        Move(movementSpeed);
    }

    void OnTriggerEnter2D(Collider2D col){

        if (col.gameObject.tag == "Health"){

            if (playerLife <= 5){

                playerLife++;
            }
        }
    }   
    void OnCollisionEnter2D(Collision2D col){

        if (col.gameObject.tag == "Enemy"){

           playerLife--;
           Destroy(col.gameObject);
        }

        if (col.gameObject.tag == "Boss"){

            Die();
        }
    }

    
    #endregion

    #region Player Functions

    void Move(float moveSpeed){

        transform.Translate(Vector2.right * Time.fixedDeltaTime * movementSpeed);
    }
    
    void Die(){

        Destroy(this.gameObject);
        GameManager.instance.GameOver();
    }
    #endregion
    
}

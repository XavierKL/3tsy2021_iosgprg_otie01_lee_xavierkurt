using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{   

    public Transform spawnPoint;
    public Spawn[] enemyPrefabs;
    public GameObject boss;
    public bool spawnBoss;
    public bool bossIsAlive;
    public int hitsBeforeDeath;
    public PlayerInput scoreReference;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawn());
        spawnBoss = false;
        bossIsAlive = false;
        scoreReference = GameObject.FindGameObjectWithTag("Player Range").GetComponent<PlayerInput>();
    }

    void Update(){

        if((scoreReference.score % 100 == 0 && scoreReference.score > 0) && !bossIsAlive){

            bossIsAlive = true;
            spawnBoss = true;
            Instantiate(boss, spawnPoint.position, spawnPoint.rotation);
        }

        if (hitsBeforeDeath >= 5){

            Destroy(GameObject.FindGameObjectWithTag("Boss"));
            GameManager.instance.GameOver();
        }
    }

    IEnumerator StartSpawn(){

        while (!spawnBoss){

            if (spawnBoss) break; // add checker for boss spawn 

            // chckers should be here

            int randomNumber = Random.Range(0, 100);

            for (int j = 0; j < enemyPrefabs.Length; j++){

                if (randomNumber >= enemyPrefabs[j].minProbability && randomNumber <= enemyPrefabs[j].maxProbability){

                    Instantiate(enemyPrefabs[j].spawnEnemy, spawnPoint.transform.position, spawnPoint.transform.rotation);
                    break;
                }
            }

            
            yield return new WaitForSeconds(2.0f);
        }
    }

    public void Boss(){


    }
    //code repurposed from previous proj
    [System.Serializable]
    public class Spawn{

        public GameObject spawnEnemy;
        public int minProbability = 0;
        public int maxProbability = 0;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    public GameObject characterPanel;
    public int characterNumber;
    public TextMeshProUGUI scoreText;

    private void Awake(){

        if (instance != null){


            Destroy(this.gameObject);
        }

        else {

            instance = this;
        }
    }

    #region unity functions

    // Start is called before the first frame update
    void Start()
    {
        
        Time.timeScale = 0;
        characterPanel.SetActive(false);
        scoreText.text = "Highest Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #endregion

    #region UI Functions

    public void GameOver(){

        Time.timeScale = 0;
        SceneManager.LoadScene(2);
        ShowScore();
    }

    public void StartGame(){

        SceneManager.LoadScene(1);
    }

    public void OnStartButtonClicked(){

        characterPanel.SetActive(true);
    }

    public void OnRetryButtonClicked(){

        SceneManager.LoadScene(0);
    }
    /*public void SetCharNumber(int number){

        characterNumber = number;
    }*/
    
    public void ResetScore(){

        PlayerPrefs.DeleteKey("HighScore");
    }

    public void ShowScore(){

        scoreText.text = "Highest Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    public void OnBlueSelect(){

        Time.timeScale = 1;
        characterNumber = 0;
        PlayerPrefs.SetInt("selectedCharacter", characterNumber);
        characterPanel.SetActive(false);
        Debug.Log(characterNumber);
        StartGame();
    }

     public void OnYellowSelect(){

        Time.timeScale = 1;
        characterNumber = 1;
        PlayerPrefs.SetInt("selectedCharacter", characterNumber);
        characterPanel.SetActive(false);
        Debug.Log(characterNumber);
        StartGame();
    }
    #endregion
}

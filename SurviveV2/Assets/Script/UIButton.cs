using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public bool pointerDown;

    public GameObject[] playerInventory;
    public GameObject activeGun;

    [SerializeField]
    private UnityEvent onHoldDown;

    public void OnPointerDown(PointerEventData eventData)
    {

        pointerDown = true;

        if (playerInventory[0].activeSelf){

            activeGun = playerInventory[0];
            activeGun.GetComponent<Gun>().Fire();
        }

        if (playerInventory[1].activeSelf){

            activeGun = playerInventory[1];
            //Debug.Log("Coroutine Active");
            //activeGun.GetComponent<Gun>().Fire();
            StartCoroutine(FullAuto());
            Debug.Log(activeGun.GetComponent<Gun>().bulletSpeed);
            //Debug.Log("fire fire");
        }

        if (playerInventory[2].activeSelf){

            activeGun = playerInventory[2];
            activeGun.GetComponent<Gun>().Fire();
        }

        
        //Debug.Log("Button is being held down");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerDown = false;
        
    }
    
    public IEnumerator FullAuto(){

        while(true){

            while((pointerDown && !activeGun.GetComponent<Gun>().isReloading) && activeGun.GetComponent<Gun>().currentClip > 0){

                activeGun.GetComponent<Gun>().Fire();
                yield return new WaitForSeconds(.1f);

                if(!pointerDown){
                    
                    Debug.Log("Coroutine stopped");
                    yield break;
                }
            }

            //yield return null;
        }
    }


    void Update(){

        if(playerInventory[1].activeSelf){

            activeGun = playerInventory[1];
        }
    }
}

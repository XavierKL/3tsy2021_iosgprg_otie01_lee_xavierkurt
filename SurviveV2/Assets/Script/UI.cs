using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI : MonoBehaviour
{

    public GameObject[] playerInventory;
    public GameObject currentGun;
    public TextMeshProUGUI clip;
    public TextMeshProUGUI ammo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (playerInventory[0].activeSelf){

            currentGun = playerInventory[0];
        }

        if (playerInventory[1].activeSelf){

            currentGun = playerInventory[1];
        }

        if (playerInventory[2].activeSelf){

            currentGun = playerInventory[2];
        }

        clip.text = currentGun.GetComponent<Gun>().currentClip.ToString();
        ammo.text = currentGun.GetComponent<Gun>().currentAmmo.ToString();
    }
}

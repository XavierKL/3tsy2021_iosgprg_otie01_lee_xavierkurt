using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    public GameObject[] patrolPoints;
    public Transform self;
    private int destPoints;
    private NavMeshAgent enemy;
    bool isNear;
    private Rigidbody body;

    public Gun gunReference;   
    public Transform target;

    float attackRadius = 20f;
    int currentPoint; 

    float timer;

    // Start is called before the first frame update
    void Start()
    {
        enemy = this.GetComponentInParent<NavMeshAgent>();
        enemy.autoBraking = false;
        isNear = false;    
        body = this.GetComponent<Rigidbody>();

        patrolPoints = GameObject.FindGameObjectsWithTag("Points");
        //gunReference = gameObject.GetComponentInChildren<Gun>();

        GoToPoint();
        StartCoroutine(Attack());
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!enemy.pathPending && enemy.remainingDistance < .5f){

             GoToPoint();
         }

        //Debug.Log(gunReference.currentClip);
    }

    void FixedUpdate(){

        Vector3 targetDelta = target.position - self.transform.position;
        Quaternion rotation = Quaternion.LookRotation(targetDelta);
        self.transform.rotation = Quaternion.Lerp(self.transform.rotation, rotation, .7f * Time.fixedDeltaTime);

        /*float distanceTo = Vector3.Distance(transform.position, target.position);

        
        if (distanceTo > attackRadius){

            GoBackToPoint();
        }*/
    }

    void GoToPoint(){

        if (patrolPoints.Length == 0)
        return;

        enemy.destination = patrolPoints[destPoints].transform.position;

        destPoints = Random.Range(0, patrolPoints.Length);
    }

    void GoBackToPoint(){

        if(!isNear && enemy.remainingDistance < 8.5f){

            enemy.destination = patrolPoints[currentPoint].transform.position;
            UpdatePosition();
        }
    }

    void UpdatePosition(){


        if (currentPoint == patrolPoints.Length - 1){

            currentPoint = 0;
        }

        else {

            currentPoint = Random.Range(0, patrolPoints.Length - 2);
        }

        Debug.Log(currentPoint);
    }
    void OnTriggerEnter(Collider col){

        if(col.gameObject.tag == "Player"){

            enemy.isStopped = true;
            target = col.gameObject.transform;
            isNear = true;
        }

        if(col.gameObject.tag == "Enemy"){

            enemy.isStopped = true;
            target = col.gameObject.transform;
            isNear = true;
        }
    }

    void OnTriggerExit(Collider col){

        if(col.gameObject.tag == "Player"){

            enemy.isStopped = false;
            target = null;
            isNear = false;
        }

        if(col.gameObject.tag == "Enemy"){

            enemy.isStopped = false;
            target = col.gameObject.transform;
            isNear = false;
        }
    }

    void Die(){

        Destroy(this.gameObject);
    }

    public IEnumerator Attack(){

        while(true){

            while(isNear && !gunReference.isReloading){

                gunReference.Fire();

                if(gunReference.currentClip <= 0){

                    StartCoroutine(gunReference.EnemyReload(gunReference.reloadTime));
                }

                Debug.Log(gunReference.currentClip);
                yield return new WaitForSeconds(gunReference.fireRate);
            }

            yield return null;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour
{

    public Health playerHealth;
    float damage;

    public Image healthBar;
    // Start is called before the first frame update
    void Start()
    {
        playerHealth = this.GetComponent<Health>();
        HealthBarRefil();
    }

    // Update is called once per frame
    void Update()
    {
        
        HealthBarRefil();

        if (playerHealth.currentHealth <= 0){

            Die();
        }
    }

    void OnTriggerEnter(Collider col){

        if(col.gameObject.tag == "Projectile"){

            switch (col.gameObject.GetComponent<Gun>().gunType){

                case GunType.pistol:
                damage = col.gameObject.GetComponent<Gun>().damage;
                break;

                case GunType.shotgun:
                damage = col.gameObject.GetComponent<Gun>().damage;
                break;

                case GunType.rifle:
                damage = col.gameObject.GetComponent<Gun>().damage;
                break;

                default: 
                break;
            }

            playerHealth.currentHealth -= damage;
        }
    }

    void Die(){

        Destroy(this.gameObject);
        GameManager.instance.GameOver();
    }

    void HealthBarRefil(){

        healthBar.fillAmount = playerHealth.currentHealth / playerHealth.maxHealth;
    }
}

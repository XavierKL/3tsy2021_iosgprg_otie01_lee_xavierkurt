using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum characterType{

    player,
    enemy
}
public class Movement : MonoBehaviour
{

    public characterType character;

    float movementSpeed;
    public Joystick moveJoystick;
    public Joystick lookJoystick;

    Vector3 movement;

    public Rigidbody body;

    // Start is called before the first frame update
    void Start()
    {
        
        movementSpeed = 5.0f;
    }

    void Update(){

        if(this.character == characterType.player){

            movement.x = moveJoystick.Horizontal;
            movement.z = moveJoystick.Vertical;

            if(lookJoystick.Horizontal != 0 || lookJoystick.Vertical != 0){

                Vector3 lookVector = new Vector3(lookJoystick.Horizontal, 0 , lookJoystick.Vertical);

                lookVector = lookVector.normalized;

                transform.rotation = Quaternion.LookRotation(lookVector);

            }
        }

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        body.MovePosition(body.position + movement * movementSpeed * Time.fixedDeltaTime);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform[] spawners;
    public GameObject[] enemyPrefab;

    int randomSpawnPoint;
    int randomEnemyNumber;

    bool startSpawning;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        startSpawning = false;
        timer = 0f;

        for(int i = 0; i < spawners.Length; i++){

            Spawn();
        }

        StartCoroutine(StartSpawn());
    }

    // Update is called once per frame
    void Update()
    {
        
        timer += Time.deltaTime;

        if (timer >= 5f){

            startSpawning = true;
        }
    }

    IEnumerator StartSpawn(){


        while (true){
            
            while(startSpawning){
                
                Spawn();
                yield return new WaitForSeconds(2f);
            }
            yield return null;
        }
    }

    void Spawn(){

        randomEnemyNumber = Random.Range(0, enemyPrefab.Length - 1);
        randomSpawnPoint = Random.Range(0, spawners.Length - 1);

        int currentSpawnPoint = randomSpawnPoint;

        if(currentSpawnPoint == randomSpawnPoint){

            randomSpawnPoint = Random.Range(0, spawners.Length - 1);
        }
                           
        GameObject enemy = Instantiate(enemyPrefab[randomEnemyNumber], spawners[randomSpawnPoint].position, Quaternion.identity);  
    } 
}

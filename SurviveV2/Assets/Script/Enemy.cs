using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{   

    public Health health;
    float damage;

    // Start is called before the first frame update
    void Start()
    {

        health = this.GetComponent<Health>();

        //GoToPoint();
        //StartCoroutine(Attack());
    }

    // Update is called once per frame
    void Update()
    {
        
       
        if(this.health.currentHealth <= 0f){

            Die();
        }

    }

    // void OnTriggerEnter(Collider col){

    //     if(col.gameObject.tag == "Projectile"){

    //         switch (col.gameObject.GetComponent<Gun>().gunType){

    //             case GunType.pistol:
    //             damage = col.gameObject.GetComponent<Gun>().damage;
    //             break;

    //             case GunType.shotgun:
    //             damage = col.gameObject.GetComponent<Gun>().damage;
    //             break;

    //             case GunType.rifle:
    //             damage = col.gameObject.GetComponent<Gun>().damage;
    //             break;

    //             default: 
    //             break;
    //         }

    //         health.currentHealth -= damage;
    //     }
    // }

    void Die(){

        Destroy(this.gameObject);
    }

}

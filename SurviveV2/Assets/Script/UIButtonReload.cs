using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class UIButtonReload : MonoBehaviour, IPointerDownHandler
{

    public GameObject[] playerInventory;
    public GameObject activeGun;

    public void OnPointerDown(PointerEventData eventData)
    {

        if(playerInventory[0].activeSelf){

            activeGun = playerInventory[0];
        }

        if(playerInventory[1].activeSelf){

            activeGun = playerInventory[1];
        }

        if(playerInventory[2].activeSelf){

            activeGun = playerInventory[2];
        }
        
        StartCoroutine(activeGun.GetComponent<Gun>().Reload());
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

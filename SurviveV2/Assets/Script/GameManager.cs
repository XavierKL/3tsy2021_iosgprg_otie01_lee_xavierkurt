using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    private void Awake(){

        if (instance != null){

            Destroy(this.gameObject);
        }

        else {

            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

   public void StartGame(){

       Time.timeScale = 1;
       SceneManager.LoadScene(1);
   }

   public void GameOver(){

       SceneManager.LoadScene(2);
       Time.timeScale = 0;
   }
   
   public void GoBackToMenu(){

       SceneManager.LoadScene(0);
   }
}

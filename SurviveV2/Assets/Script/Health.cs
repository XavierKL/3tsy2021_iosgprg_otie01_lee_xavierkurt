using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Health : MonoBehaviour
{

    public float currentHealth;
    public float maxHealth;

    // Start is called before the first frame update
    void Start()
    {
        maxHealth = 100;
        currentHealth = maxHealth;        
    }

    void OnColliderEnter(Collision col){

        if(col.gameObject.tag == "Rifle round"){
            
            //Debug.Log("Hit");
            this.currentHealth -= 15f;
        }

        if(col.gameObject.tag == "Pistol round"){
            
            //Debug.Log("Hit");
            this.currentHealth -= 10f;
        }

        if(col.gameObject.tag == "Shotgun pellet"){
            
            //Debug.Log("Hit");
            this.currentHealth -= 20f;
        }
    }
}

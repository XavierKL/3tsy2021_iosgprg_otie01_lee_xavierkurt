using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ammoType{

    pistolAmmo,
    shotgunAmmo,
    rifleAmmo
}
public class Ammo : MonoBehaviour
{
    
    public ammoType typeOfAmmo;
    Gun gunReference;
    
    void OnTriggerEnter(Collider col){

        if(col.gameObject.tag == "Player"){
            
            gunReference = col.GetComponentInChildren<Gun>();

            if(this.typeOfAmmo == ammoType.pistolAmmo && gunReference.gunType == GunType.pistol){

                gunReference.currentAmmo += 15;
            }

            if(this.typeOfAmmo == ammoType.rifleAmmo && gunReference.gunType == GunType.rifle){

                gunReference.currentAmmo += 30;
            }

            if(this.typeOfAmmo == ammoType.shotgunAmmo && gunReference.gunType == GunType.shotgun){

                gunReference.currentAmmo += 2;
            }
            
            Destroy(this.gameObject);
        }
    }
}

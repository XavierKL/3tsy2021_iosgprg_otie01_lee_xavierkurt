using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIButtonSwap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{   

    public GameObject[] playerInventory;
    public int selectedWeapon;
    public bool pointerDown;



    public void OnPointerDown(PointerEventData eventData)
    {

        int swithcWeapon = selectedWeapon;
        swithcWeapon++;
        swithcWeapon %= playerInventory.Length;
        SelectedWeapon(swithcWeapon);
    }

    public void OnPointerUp(PointerEventData eventData)
    {

        //pointerDown = false;
    }
    // Start is called before the first frame update
    void Start()
    {
        
        selectedWeapon = 0;
        playerInventory[selectedWeapon].gameObject.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SelectedWeapon(int i){

        playerInventory[selectedWeapon].gameObject.SetActive(false);
        playerInventory[i].gameObject.SetActive(true);
        selectedWeapon = i;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public GameObject hitEffect;
    public Gun damageReference;
    public GunType ammoType;
    public float damage;

    void Start(){

        Invoke("AutoDestroy", 1f);
        
        damageReference = this.GetComponent<Gun>();
        
    }
    void OnTriggerEnter(Collider col){

        if (col.gameObject.tag == "Player"){

            Debug.Log(col.gameObject.tag);

            if(this.ammoType == GunType.pistol){

            damage = damageReference.damage;
            }

            if(this.ammoType == GunType.shotgun){

                damage = damageReference.damage;
            }

            if(this.ammoType == GunType.rifle){

                damage = damageReference.damage;
            }
            
            col.gameObject.GetComponent<Health>().currentHealth -= damage;
            Destroy(this.gameObject);
        }

        if (col.gameObject.tag == "Enemy"){

            Debug.Log(col.gameObject.tag);
            
            if(this.ammoType == GunType.pistol){

            damage = damageReference.damage;
            }

            if(this.ammoType == GunType.shotgun){

                damage = damageReference.damage;
            }

            if(this.ammoType == GunType.rifle){

                damage = damageReference.damage;
            }
            col.gameObject.GetComponent<Health>().currentHealth -= damage;
            Destroy(this.gameObject);
        }

        if (col.gameObject.tag == "Wall"){

            Debug.Log(col.gameObject.tag);
            Destroy(this.gameObject);
        } 
    }

    void AutoDestroy(){

        Destroy(gameObject);
    }

   
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum GunType{

    pistol,
    shotgun,
    rifle
}

public class Gun : MonoBehaviour
{
    
    public GunType gunType;
    public float damage;
    public float fireRate;
    public int currentClip; //  current rounds in gun  
    public int maxClip; // max rounds in gun
    public int currentAmmo; // current ammount of ammo in inventory
    public int maxAmmo; // max amount of ammo player can carry 
    public int gunIndex;
    public Transform firePoint;
    public Transform[] firePoints;
    public float range;
    public float reloadTime;
    public float bulletSpeed = 25f;
    public ParticleSystem muzzleFlash;

    public GameObject bulletPrefab;
    public bool isReloading = false;

    Rigidbody body;
    Player playerReference;
    Enemy enemyReference;

    public UIButton buttonReference;
    // Start is called before the first frame update
    void Start()
    {
        
        switch(gunType){

            case GunType.pistol:
            damage = 10f;
            fireRate = .5f;
            maxClip = 15;
            currentAmmo = 15;
            maxAmmo = 60;
            range = 200f;
            currentClip = maxClip;
            gunIndex = 0;
            reloadTime = 1f;
            break;

            case GunType.shotgun:
            damage = 5f;
            fireRate = 1.0f;
            maxClip = 2;
            currentAmmo = 2;
            maxAmmo = 20;
            range = 1f;
            currentClip = maxClip;
            gunIndex = 2;
            reloadTime = 2.5f;
            break;

            case GunType.rifle:
            damage = 15f;
            fireRate = .1f;
            maxClip = 30;
            currentAmmo = 15;
            maxAmmo = 120;
            range = 50f;
            currentClip = maxClip;
            gunIndex = 1;
            reloadTime = 3f;
            break;

            default:
            break;
        }


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Fire(){
        
        if(!isReloading && currentClip > 0){

            switch(this.gunType){

                case GunType.pistol:
                GameObject bullet = Instantiate(bulletPrefab, firePoint.position, bulletPrefab.transform.rotation);
                body = bullet.GetComponent<Rigidbody>();
                body.AddForce(firePoint.forward * 20f, ForceMode.Impulse);
                currentClip--;
                break;

                case GunType.shotgun:

                    for (int i = 0; i < 3; i++){
                       
                        GameObject pellet = Instantiate(bulletPrefab, firePoints[i].position, bulletPrefab.transform.rotation);
                        body = pellet.GetComponent<Rigidbody>();
                        body.AddForce(firePoints[i].forward * 5f, ForceMode.Impulse);
                        
                        Destroy(pellet, .5f);
                    }
                    currentClip--;
                break;

                case GunType.rifle:
                GameObject rifleRound = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                body = rifleRound.GetComponent<Rigidbody>();
                body.AddForce(firePoint.forward * bulletSpeed, ForceMode.Impulse);
                currentClip--;
                break;

                default:
                break;
            }
        }
    }

    public IEnumerator FullAuto(){

        while(true){

            while((buttonReference.pointerDown && !isReloading) && currentClip > 0){

                GameObject rifleRound = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
                body = rifleRound.GetComponent<Rigidbody>();
                body.AddForce(firePoint.forward * 100f, ForceMode.Impulse);

                if(!buttonReference.pointerDown){

                    break;
                }
                //Debug.Log("Gun is firing");
                currentClip--;

        
                yield return new WaitForSeconds(.1f);
            }

            yield return null;
        }
    }

    public IEnumerator Reload(){

        if(currentClip != maxClip){

            isReloading = true;
            yield return new WaitForSeconds(reloadTime);
            
            if(currentAmmo >= maxClip){

                currentAmmo -= maxClip;
                currentClip = maxClip;
            }

            else {

                currentClip += currentAmmo;
                currentAmmo = 0;
            }

            isReloading = false;
        }

    }

    public IEnumerator EnemyReload(float time){


        isReloading = true;
        yield return new WaitForSeconds(time);

        currentClip = maxClip;
        isReloading = false;
    }

    
}

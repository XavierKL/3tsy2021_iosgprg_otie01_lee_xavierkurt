using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoSpawn : MonoBehaviour
{

    public Transform[] ammoLocations;
    public GameObject[] ammoPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
        StartCoroutine(AmmoSpawning());
    }

    IEnumerator AmmoSpawning(){

        while (true){

            int ammoNumber = Random.Range(0, ammoPrefab.Length - 1);
            int ammoLocationNumber = Random.Range(0, ammoLocations.Length);

            GameObject ammo = Instantiate(ammoPrefab[ammoNumber], ammoLocations[ammoLocationNumber].position, Quaternion.identity);
            yield return new WaitForSeconds(5f);

        }
    }
}
